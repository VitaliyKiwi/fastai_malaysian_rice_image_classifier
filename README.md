Image classification of malaysian rice cuisine images using fastai library

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/nghweigeok%2Ffastai_malaysian_rice_image_classifier/master?urlpath=%2Fvoila%2Frender%2Fimage_classification_model_prediction.ipynb)

Short video demo: https://www.loom.com/share/0667e10601724d5b85c3670e54562eee


Step 1: Create a dataset by downloading images from Google based on keywords (download_google_images.ipynb)

Step 2: Train an image classifier using Fastai2 library (image_classification_model_training.ipynb) and export the trained model (export.pkl)

Step 3: Create a web app based on Jupyter Notebook, deploy using the Voila library and host on Binder (image_classification_model_prediction.ipynb)


Some tips for hosting on Binder:

1. Make sure the repo contains the Jupyter Notebook, the trained model and requirements.txt.

2. Replace "from utils import *" from the original script to "from fastai.vision.all import *" (make sure you are either using the fastai or fastai2 library, make them consistent with the requirements.txt).

3. Go to https://mybinder.org/. Debug first before deploy. Information for debug:
- Repo type: GitLab.com
- Repo URL: https://gitlab.com/nghweigeok/fastai_malaysian_rice_image_classifier
- Path to a notebook file: (leave blank)
- Path type: URL
- Click launch. Jupyter Notebook will appear and you can debug your code in the Binder docker.

4. Host webapp on Binder: 
- Repo type: GitLab.com
- Repo URL: https://gitlab.com/nghweigeok/fastai_malaysian_rice_image_classifier
- Path to a notebook file: /voila/render/image_classification_model_prediction.ipynb
- Path type: URL
- Click launch.